/* differential/diff_Evol.c
 * 
 * Copyright (C) 2018 Abdellaziz Mohamed Amine, Boukhari Samah, Boukef Abderrahmene, Noureddine Anis
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "cuckoo.h" 

int main(int argc, char *argv[])
{
    if(argc>= 3)
    {
	char car=' ';
	float temps;
	clock_t t1, t2;
	t1= clock();

	FILE* fichier;
	fichier = fopen(argv[1] ,"r");             //entrer le nom du fichierfichier contenant l'instance */
	if (fichier != NULL){
	    int compteur =0;
	    char string[256];
	    //Boucle qui sert à compter le nombre de lignes composant l'en-tête des fichiers tsp
	    //Sert à sauter l'en-tête directement à la partie données lors de la lecture des coordonnées
	    while(car!='1'){
		compteur++;
		fgets(string, 256, fichier);
		car = string[0];
	    }
	    rewind(fichier);
	    //On saute les lignes constituant l'en-tête
	    for(int i=1; i<compteur; i++)
		fgets(string, 256, fichier);

	    sscanf(argv[2], "%d", &N);

	    float tab[N];
	    float tabx[N];
	    float taby[N];
	    compteur = 0;
	    //Lectures des sonnées de l'instance tsp
	    while(!feof(fichier) && compteur < N)
	    {
		fscanf(fichier,"%f  %f %f\n",tab+compteur,tabx+compteur,taby+compteur);
		compteur++;
	    }
	    fclose(fichier);

	    /*  initialiser la matrice des distances a partir du fichier 
		tsplib (les distances  sont euclidiennes)  */
	    float mat_dis[N][N];
	    for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
		    mat_dis[i][j]=distance_euclidienne(tabx,taby,i,j);
		}
	    }

	    int nb_iter_total=500;               //nbre max d'iterations
	    int population[taille_pop][N];       //le coucou initial
	    int pop[taille_pop][N];             //le nouveau coucou obtenu par levy
	    float y;
	    float fmin;
	    float fnew;
	    float fbest=pow(10,10);
	    srand(time(NULL));

	    int nb_it=0;
	    generer_pop(taille_pop,population);  /*initialisation--> population initiale aleatoire */
	    float fo[taille_pop];
	    for(int  i=0;i<taille_pop;i++)
		fo[i]=distance(population[i],mat_dis);


	    int l=meilleur_individu(population,mat_dis,fo);   //meilleur individu de cette population
	    fmin=fo[l];
	    if(fmin<fbest)
		fbest=fmin;

	    while(nb_it<=nb_iter_total){                              /*tant que critere d'arret faire */

		for(int  i=0;i<taille_pop;i++ ){

		    for(int  j=0;j<N;j++ ){
			if(pop[i][j]!=population[i][j])
			    pop[i][j]=population[i][j];
		    }
		}

		for(int i=0;i<taille_pop;i++){
		    y=fo[i];
		    generer_nouvelle_sol(pop,i,mat_dis,&fo[i]);    /* creer les coucous poussins et les evaluer*/
		    if(fo[i]<y){
			for(int j=0;j<N;j++){
			    if(pop[i][j]!=population[i][j])
				population[i][j]=pop[i][j];
			}                                         //  et les comparer avec leurs parents */
		    }
		    else fo[i]=y;
		}

		l=meilleur_individu(population,mat_dis,fo);

		fmin=fo[l];
		if(fmin<fbest)
		    fbest=fmin;


		for(int  i=0;i<taille_pop;i++ ){

		    if(i!=meilleur_individu(population,mat_dis,fo))
			modifier_pa(population,i,mat_dis,&fo[i]);

		}
		/*modifier une fraction des mauvais nids */


		l=meilleur_individu(population,mat_dis,fo);


		fnew=fo[l];

		if(fnew<fbest)                                       /*prendre le meilleur coucou de la generation */
		    fbest=fnew;

		nb_it=nb_it+1;                                                /* nouvelle generation */
	    }


	    t2= clock();
	    temps= (float)(t2-t1)/CLOCKS_PER_SEC;
	    char buff[70];
	    time_t date = time(NULL);


	    fichier = fopen("resultats.log","a");           //écriture des résultats dans un fichier resultats.log
	    FILE* fichier2=NULL;
	    fichier2 = fopen("tournees.log","a");           //écriture des tournées trouvées dans un fichier tournees.log
	    if (fichier != NULL){
		fprintf(fichier, "\n\n========================================================================");
		fprintf(fichier2, "\n\n========================================================================");

		setlocale(LC_TIME,"fr_FR.utf8");
		strftime(buff, sizeof buff, "%A %d %B %Y %H:%M:%S", gmtime(&date));

		fprintf(fichier, "\n\n%s", buff);
		fprintf(fichier2, "\n\n%s", buff);

		fprintf(fichier, "\nInstance: %s", argv[1]);
		fprintf(fichier2, "\nInstance: %s", argv[1]);
		printf("Instance: %s", argv[1]);

		fprintf(fichier, "\nRésultat: %.2f", fbest);
		printf("\nRésultat: %.2f", fbest);

		l=meilleur_individu(population,mat_dis,fo);
		fprintf(fichier2, "\nLa tournee trouvee est: ");
		afficher(fichier2, N, population[l]);
		fprintf(fichier, "\nTemps d'execution: %.2f s\n", temps);
		printf("\nTemps d'execution: %.2f s\n", temps);
		fclose(fichier);
		fclose(fichier2);
		return 0;
	    }
	    else{
		printf("Problème lors de l'ouverture du fichier instance\n");
		return 2;
	    }
	}
	else{
	    printf("Problème lors de l'ouverture du fichier instance\n");
	    return 2;
	}
    }
    else if(argc == 2)
    {
	printf("Veuillez préciser la taille de l'instance (le nombre de villes). Rappelez-vous le synopsis du programme:\n\t./cuckoo nom-du-fichier taille-instance\n");
	return 1;
    }
    else{
	printf("Veuillez préciser le fichier contenant l'instance + la taille (le nombre de villes) de cette dernière. Rappelez-vous le synopsis du programme:\n\t./cuckoo nom-du-fichier taille-instance\n");
	return 1;
    }
}
