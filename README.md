## Copyright

    Copyright (C) 2018 Abdellaziz Mohamed Amine, Boukef Abderrahmene,
	Boukhari Samah, Noureddine Anis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or (at
    your option) any later version.
 
    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.
 
    You should have received a copy of the GNU General Public License 
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
## Introduction
 
 This is the source code for a group project we did during the first year of our master's degree in 
 Operations Research at the USTHB, Algiers.
 
 The title of the project was:

 ## The Cuckoo metaheuristic and its adaptation to the TSP

 The objectif was to study and program the metaheuristic and apply it to teh Traveling Salesman Problem (TSP)
 and monitor its performance.

 If you want to learn more please visit 
 [my website](https://abdellazizamine.wordpress.com/projects/cuckoo-metaheuristic/).

 You can get the TSP benchmarks at [this address](http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/).
 The credits go to Gerhard Reinelt, University of Heidelberg.

## Compilation

 To compile the program execute the following command

```$ make```

 To delete the generated object files use 

```$ make clear```
 and to delete the executable and object files use

```$ make clean```

## Usage

An example of execution of the program

```$ ./cuckoo tsp/berlin52.tsp 52```

where the files of the instances are to be found in the *tsp/* folder. At each executions the solutions and their costs are cancatenated to the files *tournees.log* and *resultats.log* respectively. If the files do not exist they will be created.
