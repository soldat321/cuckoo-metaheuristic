/* differential/diff_Evol.c
 * 
 * Copyright (C) 2018 Abdellaziz Mohamed Amine, Boukhari Samah, Boukef Abderrahmene, Noureddine Anis
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<locale.h>
#include<math.h>
#include<gsl/gsl_randist.h>
#include<gsl/gsl_rng.h>


int N;  	/* Nombre de villes; taille de l'instance */
static const int taille_pop=25;                        /*taille de la population*/
static const float pa=0.2;                                   /*portion des plus mauvais nids*/    
static const float pc=0.6;                                   /* proportion de coucous intelligents */ 


int distance_euclidienne(float tabx[] ,float taby[],int m,int n);
void generer_ind(int ind[]);
void generer_pop(int n,int pop[taille_pop][N]);
int cuckoo_intel(int m);
void afficher(FILE* file, int n,int T[]);
float distance(int ind[],float S[N][N]);
void ordonnerTableau(int d[4]);
int meilleur_individu(int population[taille_pop][N],float S[N][N],float fit[taille_pop]);
int mauvais_individu(int population[taille_pop][N],float S[N][N],float fit[taille_pop][N]);
void two_opt(int population[taille_pop][N],int m,float S[N][N], float *fitness);
void eloigner(int m, int population[taille_pop][N],float S[N][N],float *fitness);
void local_voi(int m,int population[taille_pop][N],float S[N][N],float *fitness);
void generer_nouvelle_sol(int population[taille_pop][N],int m,float S[N][N],float *fitness);
void modifier_pa(int population[taille_pop][N],int m,float S[N][N],float *fit);





