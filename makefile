objects = main.o cuckoo.o

install: $(objects) 
	gcc -lm -O2 -lgsl -lgslcblas $(objects) -o cuckoo 

$(objects): cuckoo.h

clean:
	@rm -f cuckoo *.o .*[!.] 

clear:
	@rm -f *.o .*[!.] 
