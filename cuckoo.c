/* differential/diff_Evol.c
 * 
 * Copyright (C) 2018 Abdellaziz Mohamed Amine, Boukhari Samah, Boukef Abderrahmene, Noureddine Anis
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include"cuckoo.h"

/*      calculer la distance entre 2 villes         */

int distance_euclidienne(float tabx[],float taby[],int m,int n)
{
    int dist=sqrt(pow(tabx[m]-tabx[n],2)+pow(taby[m]-taby[n],2))+0.5;
    return dist;
}

/*--------------------------------------------------------------------------*/

/*  fonction qui nous permet de generer une solution d'un individu  */

void generer_ind(int ind[]){
    int i,j;
    ind[0]=0;
    for(i=1;i<N;i++){
	ind[i]=(rand()%(N-1)+1);
	for(j=1;j<i;j++){
	    if(ind[j]==ind[i])
		i--;
	}
    }
}

/*---------------------------------------------------------------------------*/

/*     generer une population aleatoirement      */

void generer_pop(int n,int pop[taille_pop][N]){
    int i;
    for( i=0;i<n;i++)
	generer_ind(pop[i]);
}

/*--------------------------------------------------------------------------*/

/*      afficher un individu de la population       */

void afficher(FILE* file, int n,int T[]){
    int i;
    fprintf(file, "\n");
    for( i=0;i<n;i++)
	fprintf(file,"%d\t",T[i]+1);
}

/*--------------------------------------------------------------------------*/

/*      calcule la distance (le cout ) d'un individu    */

float distance(int ind[],float S[N][N]){
    float Dist=0;
    int i;
    for(i=0;i<N-1;i++)
	Dist+=S[ind[i]][ind[i+1]];
    Dist+=S[ind[0]][ind[N-1]];            // on ajoute le cout de retour

    return Dist;             }

    /*--------------------------------------------------------------------------*/
    /* cette fonction nous permet d'identifier les cuckoos intelligents  */

    int cuckoo_intel(int m){
	int est_inetelligent;
	if((float) rand()/RAND_MAX>pc)
	    est_inetelligent=1;
	else est_inetelligent=0;
	return est_inetelligent;
    }



/*--------------------------------------------------------------------------*/

/*    fonction qui ordonne (par ordre croissant) les positions genere  
 *    aleatoirement pour effectuer un double-bridge   */

void ordonnerTableau(int d[4]){
    int i,t,k=0;

    for(t = 1; t < 4; t++){
	for(i=0; i < 4 - 1; i++)
	{
	    if(d[i] > d[i+1])
	    {
		k=d[i];
		d[i]=d[i+1];
		d[i+1]=k;
	    }
	}
    }
}


/*-----------------------------------------------------------------------*/

/*    cette fonction nous permet de connaitre le meilleur individu d'une 
 *    population   */

int meilleur_individu(int population[taille_pop][N],float S[N][N],float fit[taille_pop])
{
    int i,id=0;
    for(i=1;i<taille_pop;i++){
	if(fit[i]<fit[id])
	    id=i;
    }
    return id;

}

/*-----------------------------------------------------------------------*/

/*    cette fonction nous permet de connaitre le nid le plus 
 *    mauvais de la  population    */

int mauvais_individu(int population[taille_pop][N],float S[N][N],float fit[taille_pop][N])
{
    int i,id=0;
    for(i=0;i<taille_pop;i++){
	if(fit[id]>fit[i])
	    id=i;
    }
    return id;

}

/*------------------------------------------------------------------------*/

/*   cette fonction permet d'effectuer une perturbation 2-opt 
 *   de maniere  aleatoire   */

void two_opt(int population[taille_pop][N],int m,float S[N][N], float *fitness)
{

    int u,p;
    int i=rand()%(N-1)+1;
    int j=rand()%(N-1)+1;
    while( j==i )
	j=rand()%(N-1)+1;
    float y=0;
    if(j<i){
	u=i;
	i=j;
	j=u;
    }
    /*  elle nous permet egalement de calculer le gain(ou perte)
	en terme de cout suite au mouvement 2-opt */
    if(j==N-1)
	y=S[population[m][i-1]][population[m][j]]+S[population[m][i]][population[m][0]]
	    -S[population[m][i-1]][population[m][i]]-S[population[m][j]][population[m][0]];

    else
	y=S[population[m][i-1]][population[m][j]]+S[population[m][i]][population[m][j+1]]
	    -(S[population[m][i-1]][population[m][i]]+S[population[m][j]][population[m][j+1]]);

    *fitness+=y;
    u=population[m][i];
    population[m][i]=population[m][j];               /* mouvement 2-opt */
    population[m][j]=u;

    for( p=i+1;p<((j+i)/2)+1;p++){
	u=population[m][p];
	population[m][p]=population[m][i-p+j];
	population[m][i-p+j]=u;

    }


}


/*-------------------------------------------------------------------------*/

/*  cette fonction nous permet de s'eloigner de la region courrante
 *  en effectuant l'operateur double bridge  */

void eloigner(int m, int population[taille_pop][N],float S[N][N],float *fitness)
{
    int nouv[N];
    int indi[4];
    int i1,i2,i3,i4;
    float y=0;
    for(i1=0;i1<N;i1++){
	nouv[i1]=population[m][i1];

    }

    int i=rand()%(N-2)+0,a;
    int j=rand()%(N-2)+0;
    int k=rand()%(N-2)+0;
    int l=rand()%(N-2)+0;

    while(j==i ||j==i+1 ||j==i-1)
	j=rand()%(N-2)+0;
    while(k==j ||k==j+1 ||k==j-1 ||k==i ||k==i+1 ||k==i-1)
	k=rand()%(N-2)+0;
    while(l==k || l==k+1 ||l==j ||l==j+1 ||l==i ||l==i+1 ||l==k-1 ||l==j-1 || l==i-1)
	l=rand()%(N-2)+0;

    indi[0]=i;
    indi[1]=j;
    indi[2]=k;
    indi[3]=l;
    ordonnerTableau(indi);
    i=indi[0];
    j=indi[1];
    k=indi[2];
    l=indi[3];
    y=S[population[m][i]][population[m][k+1]]+S[population[m][j+1]][population[m][l]]
	+S[population[m][k]][population[m][i+1]]+S[population[m][j]][population[m][l+1]];
    y-=(S[population[m][i]][population[m][i+1]]+S[population[m][j]][population[m][j+1]]
	    +S[population[m][k]][population[m][k+1]]+S[population[m][l]][population[m][l+1]]);

    *fitness+=y;
    a=0;
    for(i1=0;i1<=i;i1++){
	population[m][i1]=nouv[i1];
	a++;
    }
    for(i2=k+1;i2<=l;i2++)
    {
	population[m][a]=nouv[i2];
	a++;}


    for(i3=j+1;i3<=k;i3++){

	population[m][a]=nouv[i3];
	a++;
    }


    for(i4=i+1;i4<=j;i4++){

	population[m][a]=nouv[i4];
	a++;
    }


}

/*-------------------------------------------------------------------------*/

/*  Cette fonction nous permet permet d'obtnir un 
 *  optimum local dans une region   */

void local_voi(int m,int population[taille_pop][N],float S[N][N],float *fitness)
{
    int u,i,p,l,j;
    int nouv[N];
    int amelioration=1;
    for(i=0;i<N;i++)
	nouv[i]=population[m][i];
    float y,o;

    o=(*fitness);
    while(amelioration==1){

	for(i=1;i<N-1;i++){

	    if((S[nouv[i-1]][nouv[i]]+S[nouv[N-1]][nouv[0]])>(S[nouv[i-1]][nouv[N-1]]+S[nouv[i]][nouv[0]])){

		y=S[nouv[i-1]][nouv[N-1]];
		y+=S[nouv[i]][nouv[0]];
		y-=S[nouv[i-1]][nouv[i]];
		y-=S[nouv[N-1]][nouv[0]];

		*fitness+=y;

		l=N-1;

		u=nouv[i];
		nouv[i]=nouv[l];
		nouv[l]=u;

		for(p=i+1;p<((l+i)/2)+1;p++){
		    u=nouv[p];
		    nouv[p]=nouv[i-p+l];
		    nouv[i-p+l]=u;
		}
	    }

	    for(j=i+1;j<N-1;j++){

		if((S[nouv[i-1]][nouv[i]]+S[nouv[j]][nouv[j+1]])>(S[nouv[i-1]][nouv[j]]+S[nouv[i]][nouv[j+1]])){
		    y=S[nouv[i-1]][nouv[j]];
		    y+=S[nouv[i]][nouv[j+1]];
		    y-=S[nouv[i-1]][nouv[i]];
		    y-=S[nouv[j]][nouv[j+1]];

		    *fitness+=y;


		    u=nouv[i];
		    nouv[i]=nouv[j];
		    nouv[j]=u;

		    for(p=i+1;p<((j+i)/2)+1;p++){
			u=nouv[p];
			nouv[p]=nouv[i-p+j];
			nouv[i-p+j]=u;}



		}
	    }
	}

	if((*fitness)==o){
	    amelioration=0;
	    for(i=0;i<N;i++){
		if(population[m][i]!=nouv[i])
		    population[m][i]=nouv[i];
	    }
	}
	else o=(*fitness);

    }

}

/*--------------------------------------------------------------------*/

/*  cette fonction permet de generer de nouveaux coucous selon 
 *  la valeur de levy  */

void generer_nouvelle_sol(int population[taille_pop][N],int m,float S[N][N],float *fitness)
{
    int j;
    float n=20;

    float i=1/(n+1);
    int k=1;
    if(cuckoo_intel(m)){
	const double alpha=1;                                 /*parametre alpha de la loi de Lévy*/
	const double c=0.01;                                 /*parametre c de la loi de Lévy*/
	gsl_rng *r = gsl_rng_alloc( gsl_rng_taus2);
	double s = gsl_ran_levy(r, c, alpha);


	while( k!=n){
	    if((k-1)*i<=fabs(s) && fabs(s)<k*i){
		for(j=0;j<k;j++)
		    two_opt(population,m,S,fitness);
		local_voi(m,population,S,fitness);
		k=n;
	    }
	    else k+=1;}

	if(fabs(s)>n*i){
	    eloigner(m,population,S,fitness);
	    local_voi(m,population,S,fitness);


	}

    }
}

/*----------------------------------------------------------------------*/

/*  nous modifions une fraction des plus mauvais 
 *  nids  */

void modifier_pa(int population[taille_pop][N],int m,float S[N][N],float *fit)
{
    float s;
    s=(float) rand()/RAND_MAX  ;
    if(s <=pa && cuckoo_intel(m)){

	eloigner(m,population,S,fit);

	local_voi(m,population,S,fit);


    }
}

